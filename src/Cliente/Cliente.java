package Cliente;

public class Cliente {
	private int num_cuenta;
	
	private String cedula;
	private String nombre;
	private String apellido;
	private String tipo_cuenta;
	private String usuario;
	private String contrasena;
	private float saldo_inicial;
	
	public Cliente() {
	}
	
	public Cliente(int num_cuenta, String cedula, String nombre, String apellido, String tipo_cuenta, String usuario, String contrasena,float saldo_inicial) {
		this.num_cuenta = num_cuenta;
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.tipo_cuenta = tipo_cuenta;
		this.usuario = usuario;
		this.contrasena = contrasena;
		this.saldo_inicial = saldo_inicial;
	}

	public int getNum_cuenta() {
		return num_cuenta;
	}

	public void setNum_cuenta(int num_cuenta) {
		this.num_cuenta = num_cuenta;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getTipo_cuenta() {
		return tipo_cuenta;
	}

	public void setTipo_cuenta(String tipo_cuenta) {
		this.tipo_cuenta = tipo_cuenta;
	}
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
	public float getSaldo_inicial() {
		return saldo_inicial;
	}

	public void setSaldo_inicial(float saldo_inicial) {
		this.saldo_inicial = saldo_inicial;
	}
	
	
	
	@Override
	public String toString() {
		return this.num_cuenta+", "+this.cedula+", "+this.nombre+", "+this.apellido+", "+this.tipo_cuenta+", "+this.usuario+", "+this.contrasena+", "+this.saldo_inicial;
	}
}