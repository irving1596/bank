package Vista;

import java.util.List;

import Cliente.Cliente;
 
public class Vista {
	public void verCliente(Cliente cliente) {
		System.out.println("Datos del Cliente: "+cliente);
	}
	
	public void verClientes(List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			System.out.println("Datos del Cliente: "+cliente);
		}		
	}
}
