package Controlador;

import java.util.ArrayList;
import java.util.List;
 
import Clientecrud.Clientecrud;
import InterfCliente.ICliente;
import Cliente.Cliente;
import Vista.Vista;
 
public class Controlador {
	
	private Vista vista= new Vista();
	
	public Controlador() {
	}
	
	//llama al DAO para guardar un cliente
	public void registrar(Cliente cliente ) {
		ICliente dao= new  Clientecrud();
		dao.registrar(cliente);
	}
	
	//llama al DAO para actualizar un cliente
	public void actualizar(Cliente cliente) {
		ICliente dao= new  Clientecrud();
		dao.actualizar(cliente);
	}
	
	//llama al DAO para eliminar un cliente
	public void eliminar(Cliente cliente) {
		ICliente dao= new  Clientecrud();
		dao.eliminar(cliente);
	}
	
	//llama al DAO para obtener todos los clientes y luego los muestra en la vista
	public void verClientes(){
		List<Cliente> clientes = new ArrayList<Cliente>();
		ICliente dao= new  Clientecrud();
		clientes=dao.obtener();
		vista.verClientes(clientes);
	}
}
