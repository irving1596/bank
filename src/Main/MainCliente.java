package Main;

import Controlador.Controlador;

import Cliente.Cliente;

public class MainCliente {

	public static void main(String[] args) {

		Cliente cliente = new Cliente(1717213,"8-999-0", "Edgardo", "Linares","Ahorro","LAPERRA","PUTIZORRA",(float) 1.22);			
		
		// controlador
		Controlador controller = new Controlador();

		// guarda un cliente a través del controlador
		//controller.registrar(cliente);

		// ver clientes
		controller.verClientes();

		// editar un cliente por medio del id
		/*
		cliente.setNum_cuenta(1717213);
		cliente.setNombre("Santiago");
		controller.actualizar(cliente);
		*/

		// eliminar un cliente por medio del id
		cliente.setNum_cuenta(1717213);
		controller.eliminar(cliente);
		
		controller.verClientes();
	}
}
