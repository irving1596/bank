package Clientecrud;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 
import conexion.Conexion;
import InterfCliente.ICliente;
import Cliente.Cliente;
 
public class Clientecrud implements ICliente {	
	
	@Override
	public boolean registrar(Cliente cliente) {
		boolean registrar = false;
		
		Statement stm= null;
		Connection con=null;
		
		String sql="INSERT INTO Cliente values ('"+cliente.getNum_cuenta()+"','"+cliente.getCedula()+"','"+cliente.getNombre()+"','"+cliente.getApellido()+"','"+cliente.getTipo_cuenta()+"','"+cliente.getUsuario()+"','"+cliente.getContrasena()+"','"+cliente.getSaldo_inicial()+"')";
		
		try {			
			con=Conexion.conectar();
			stm= con.createStatement();
			stm.execute(sql);
			registrar=true;
			stm.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Error: Clase ClienteDaoImple, método registrar");
			e.printStackTrace();
		}
		return registrar;
	}
 
	@Override
	public List<Cliente> obtener() {
		Connection co =null;
		Statement stm= null;
		ResultSet rs=null;
		
		String sql="SELECT * FROM Cliente ORDER BY NUM_CUENTA";
		
		List<Cliente> listaCliente= new ArrayList<Cliente>();
		
		try {			
			co= Conexion.conectar();
			stm=co.createStatement();
			rs=stm.executeQuery(sql);
			while (rs.next()) {
				Cliente c=new Cliente();
				c.setNum_cuenta(rs.getInt(1));
				c.setCedula(rs.getString(2));
				c.setNombre(rs.getString(3));
				c.setApellido(rs.getString(4));
				c.setTipo_cuenta(rs.getString(5));
				c.setUsuario(rs.getString(6));
				c.setContrasena(rs.getString(7));
				c.setSaldo_inicial(rs.getFloat(8));
				
				listaCliente.add(c);
			}
			stm.close();
			rs.close();
			co.close();
		} catch (SQLException e) {
			System.out.println("Error: Clase ClienteDaoImple, método obtener");
			e.printStackTrace();
		}
		
		return listaCliente;
	}
 
	@Override
	public boolean actualizar(Cliente cliente) {
		Connection connect= null;
		Statement stm= null;
		
		boolean actualizar=false;
				
		String sql="UPDATE CLIENTE SET cedula='"+cliente.getCedula()+"', nombre='"+cliente.getNombre()+"', apellido='"+cliente.getApellido()+"',tipo_cuenta='"+cliente.getTipo_cuenta()+"',usuario='"+cliente.getUsuario()+"',contrasena='"+cliente.getContrasena()+"',saldo_inicial="+cliente.getSaldo_inicial()+""+" WHERE Num_cuenta="+cliente.getNum_cuenta();
		try {
			connect=Conexion.conectar();
			stm=connect.createStatement();
			stm.execute(sql);
			actualizar=true;
		} catch (SQLException e) {
			System.out.println("Error: Clase ClienteDaoImple, método actualizar");
			e.printStackTrace();
		}		
		return actualizar;
	}
 
	@Override
	public boolean eliminar(Cliente cliente) {
		Connection connect= null;
		Statement stm= null;
		
		boolean eliminar=false;
				
		String sql="DELETE FROM CLIENTE WHERE Num_cuenta="+cliente.getNum_cuenta();
		try {
			connect=Conexion.conectar();
			stm=connect.createStatement();
			stm.execute(sql);
			eliminar=true;
		} catch (SQLException e) {
			System.out.println("Error: Clase ClienteDaoImple, método eliminar");
			e.printStackTrace();
		}		
		return eliminar;
	}
 
}