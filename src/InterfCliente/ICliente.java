package InterfCliente;
import java.util.List;

import Cliente.Cliente;
 
public interface ICliente {	
	public boolean registrar(Cliente cliente);
	public List<Cliente> obtener();
	public boolean actualizar(Cliente cliente);
	public boolean eliminar(Cliente cliente);
}
